#ユーザー作成
User.create!(name: "Example User",
             user_name: "Example",
             email: "example@railstutorial.org",
             password: "foobar",
             password_confirmation: "foobar",
             url: "https://example.com",
             self_introduction: "Hello,world!!",
             phone_number: "090-1234-5678",
             gender: "man",
             confirmed_at: Time.now)

99.times do |n|
  name = Faker::Name.name
  user_name = Faker::Name.first_name
  email = "example-#{n+1}@railstutorial.org"
  password = "password"
  url = "https://example_#{n+1}.com"
  self_introduction = Faker::Lorem.sentence(5)
  phone_number = "090-1234-5678"
  gender = n%3+1
  User.create!(name: name,
               user_name: user_name,
               email: email,
               password: password,
               password_confirmation: password,
               url: url,
               self_introduction: self_introduction,
               phone_number: phone_number,
               gender: gender,
               confirmed_at: Time.now)
end

#投稿作成
users = User.order(:created_at).take(6)
20.times do
  image = open("#{Rails.root}/app/assets/images/sample.jpeg")
  content = Faker::Lorem.sentence(5)
  users.each { |user| user.microposts.create!(image: image, content: content)}
end

#関係性作成
users = User.all
user = users.first
following = users[2..50]
followers = users[3..40]
following.each{ |followed| user.follow(followed) }
followers.each{ |follower| follower.follow(user) }