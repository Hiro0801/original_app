class AddColumnsToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :name, :string
    add_column :users, :user_name, :string
    add_column :users, :url, :string
    add_column :users, :self_introduction, :text
    add_column :users, :phone_number, :string
    add_column :users, :gender, :integer
  end
end
