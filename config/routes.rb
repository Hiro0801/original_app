Rails.application.routes.draw do
  root to: 'static_pages#home'
  get  '/search', to: 'static_pages#search'
  post '/search', to: 'static_pages#searching'

  devise_for :users, controllers: {
    passwords: 'users/passwords',
    registrations: 'users/registrations',
    sessions: 'users/sessions',
  }

  resources :users, only: [:show] do
    member do
      get :following, :followers
    end
  end

  resources :microposts,             only: [:show, :new, :create, :destroy]
  resources :comments,               only: [:create, :destroy]
  resources :likes,                  only: [:index, :create, :destroy]
  resources :relationships,          only: [:create, :destroy]
  resources :notifications,          only: [:index]

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
