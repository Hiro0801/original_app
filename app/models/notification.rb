class Notification < ApplicationRecord
  scope :from_new_to_old, -> { order(created_at: :desc) }
  belongs_to :visitor, class_name: "User", foreign_key: 'visitor_id'
  belongs_to :visited, class_name: "User", foreign_key: 'visited_id'
  belongs_to :micropost, optional: true
  belongs_to :comment, optional: true

  validates :visitor_id, presence: true
  validates :visited_id, presence: true
  validates :action, presence: true
end
