class Micropost < ApplicationRecord
  belongs_to :user
  has_many :comments, dependent: :destroy
  has_many :likes, dependent: :destroy
  has_many :notifications, dependent: :destroy
  scope :from_new_to_old, -> { order(created_at: :desc) }
  mount_uploader :image, ImageUploader
  validates :user_id, presence: true
  validates :image, presence: true
  validates :content, length: { maximum: 140 }
  validate :image_size

  # 検索ワードに該当する投稿があるか確認し無ければnilを返す
  def self.search(keyword)
    if keyword.present?
      if where("content LIKE ?", "%#{keyword}%").present?
        where("content LIKE ?", "%#{keyword}%")
      else
        nil
      end
    else
      nil
    end
  end

  # いいね！に対して通知を作成する
  def create_notification_like!(current_user)
    # すでに「いいね」されているか検索
    temp = Notification.where(
      "visitor_id = :visitor and visited_id = :visited and
      micropost_id= :micropost and action= :action",
      visitor: current_user, visited: user_id, micropost: id, action: "like"
    )
    # いいねされていない場合のみ、通知レコードを作成
    if temp.blank?
      notification = current_user.active_notifications.build(
        micropost_id: id,
        visited_id: user_id,
        action: 'like'
      )
      # 自分の投稿に対するいいねの場合は、通知済みとする
      if notification.visitor_id == notification.visited_id
        notification.checked = true
      end
      notification.save if notification.valid?
    end
  end

  # コメントに対して通知を作成する
  def create_notification_comment!(current_user, comment_id)
    temp = Notification.where(
      "visitor_id= :visitor and visited_id = :visited and
      comment_id = :comment and action = :action",
      visitor: current_user, visited: user_id,
      comment: comment_id, action: "comment"
    )
    if temp.blank?
      notification = current_user.active_notifications.build(
        micropost_id: id,
        comment_id: comment_id,
        visited_id: user_id,
        action: 'comment'
      )
      # 自分の投稿に対するコメントの場合は、通知済みとする
      if notification.visitor_id == notification.visited_id
        notification.checked = true
      end
      notification.save if notification.valid?
    end
  end

  private

  # アップロードされた画像のサイズをバリデーションする
  def image_size
    if image.size > 5.megabytes
      errors.add(:image, "should be less than 5MB")
    end
  end
end
