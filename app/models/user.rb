class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :confirmable

  has_many :microposts, dependent: :destroy
  has_many :comments, dependent: :destroy
  has_many :likes, dependent: :destroy
  has_many :liked_microposts, through: :likes, source: :micropost
  has_many :active_relationships, class_name: "Relationship",
                                  foreign_key: "follower_id", dependent: :destroy
  has_many :passive_relationships, class_name: "Relationship",
                                   foreign_key: "followed_id", dependent: :destroy
  has_many :following, through: :active_relationships, source: :followed
  has_many :followers, through: :passive_relationships, source: :follower
  has_many :active_notifications, class_name: "Notification",
                                  foreign_key: "visitor_id", dependent: :destroy
  has_many :passive_notifications, class_name: "Notification",
                                   foreign_key: "visited_id", dependent: :destroy

  validates :name, presence: true, length: { maximum: 50 }
  validates :user_name, presence: true, length: { maximum: 50 }
  VALID_URL_REGEX = /\A#{URI.regexp(%w(http https))}\z/.freeze
  validates :url, format: { with: VALID_URL_REGEX }, allow_blank: true
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i.freeze
  validates :email, length: { maximum: 255 }, format: { with: VALID_EMAIL_REGEX }
  VALID_PHONE_NUMBER_REGEX = /\A(((0(\d{1}[-(]?\d{4}|\d{2}[-(]?\d{3}|\d{3}[-(]?
                             \d{2}|\d{4}[-(]?\d{1}|[5789]0[-(]?\d{4})[-)]?)|\d
                             {1,4}\-?)\d{4}|0120[-(]?\d{3}[-)]?\d{3})\z/.freeze
  validates :phone_number, format: { with: VALID_PHONE_NUMBER_REGEX }, allow_blank: true
  enum gender: { man: 1, woman: 2, other: 3 }

  # ユーザーのステータスフィードを返す
  def feed
    Micropost.where(user_id: active_relationships.select(:followed_id)).
      or(Micropost.where(user_id: id))
  end

  # ユーザーをフォローする
  def follow(other_user)
    following << other_user
  end

  # ユーザーをフォロー解除する
  def unfollow(other_user)
    active_relationships.find_by(followed_id: other_user.id).destroy
  end

  # 現在のユーザーがフォローしてたらtrueを返す
  def following?(other_user)
    following.include?(other_user)
  end

  # 投稿に対して「いいね！」する
  def like(micropost)
    likes.create(micropost_id: micropost.id)
  end

  # 投稿に対して「いいね！」を取り消す
  def unlike(micropost)
    likes.find_by(micropost_id: micropost.id).destroy
  end

  # 投稿に対して「いいね！」をしているか確認
  def like?(micropost)
    likes.where(micropost_id: micropost.id).present?
  end

  # 検索ワードに該当する項目があるか確認し無ければnilを返す
  def self.search(keyword)
    if keyword.present?
      if where("name LIKE ?", "%#{keyword}%").present?
        where("name LIKE ?", "%#{keyword}%")
      else
        nil
      end
    else
      nil
    end
  end

  def create_notification_follow!(current_user)
    temp = Notification.where([
      "visitor_id = ? and visited_id = ? and action = ? ",
      current_user.id, id, 'follow',
    ])
    if temp.blank?
      notification = current_user.active_notifications.build(
        visited_id: id,
        action: 'follow'
      )
      notification.save if notification.valid?
    end
  end
end
