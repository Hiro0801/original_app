class NotificationsController < ApplicationController
  before_action :authenticate_user!

  def index
    @notifications = current_user.passive_notifications.
      where.not(visitor_id: current_user.id).
      from_new_to_old.
      paginate(page: params[:page], per_page: 10)
    @notifications.where(checked: false).each do |notification|
      notification.update_attributes(checked: true)
    end
  end
end
