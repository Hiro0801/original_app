class MicropostsController < ApplicationController
  before_action :authenticate_user!
  before_action :correct_user, only: :destroy

  def show
    @micropost = Micropost.find(params[:id])
    @comment = current_user.comments.build
    @comments = @micropost.comments.from_new_to_old.paginate(page: params[:page], per_page: 10)
  end

  def new
    @micropost = current_user.microposts.build
  end

  def create
    @micropost = current_user.microposts.build(micropost_params)
    if @micropost.save
      flash[:success] = "画像が投稿されました。"
      redirect_to @micropost
    else
      flash.now[:danger] = "正しく入力して下さい。"
      render 'new'
    end
  end

  def destroy
    @micropost.destroy
    flash[:success] = "投稿を削除しました。"
    redirect_to root_path
  end

  private

  def micropost_params
    params.require(:micropost).permit(:image, :content)
  end

  def correct_user
    @micropost = current_user.microposts.find_by(id: params[:id])
    redirect_to root_url if @micropost.nil?
  end
end
