class LikesController < ApplicationController
  before_action :authenticate_user!

  def index
    @liked_microposts = current_user.liked_microposts.from_new_to_old.
      paginate(page: params[:page], per_page: 10)
  end

  def create
    @micropost = Micropost.find(params[:micropost_id])
    current_user.like(@micropost)
    @micropost.create_notification_like!(current_user)
    respond_to do |format|
      format.html { redirect_to request.referrer || root_url }
      format.js
    end
  end

  def destroy
    @micropost = Like.find(params[:id]).micropost
    current_user.unlike(@micropost)
    respond_to do |format|
      format.html { redirect_to request.referrer || root_url }
      format.js
    end
  end
end
