class StaticPagesController < ApplicationController
  before_action :authenticate_user!, only: [:search, :searching]

  def home
    if signed_in?
      @micropost = current_user.microposts.build
      @feed_items = current_user.feed.from_new_to_old.paginate(page: params[:page], per_page: 10)
    end
  end

  def search
  end

  def searching
    @users = User.search(params[:search][:keyword])
    @microposts = Micropost.from_new_to_old.search(params[:search][:keyword])
    if @users
      render 'search'
    elsif @microposts
      render 'search'
    elsif params[:search][:keyword].present?
      flash.now[:danger] = "検索ワードに該当する項目は見つかりませんでした。"
      render 'search'
    else
      flash.now[:danger] = "検索ワードを入力して下さい。"
      render 'search'
    end
  end
end
