require 'test_helper'

class NotificationTest < ActiveSupport::TestCase
  def setup
    @notification = Notification.new(
      visited_id: users(:michael).id,
      visitor_id: users(:archer).id,
      micropost_id: microposts(:orange).id,
      comment_id: nil,
      action: "like"
    )
  end

  test "should be valid" do
    assert @notification.valid?
  end

  test "should require a visitor_id" do
    @notification.visitor_id = ""
    assert_not @notification.valid?
  end

  test "should require a visited_id" do
    @notification.visited_id = ""
    assert_not @notification.valid?
  end

  test "should not require a micropost_id" do
    @notification.micropost_id = ""
    assert @notification.valid?
  end

  test "should not require a comment_id" do
    @notification.comment_id = ""
    assert @notification.valid?
  end

  test "should require an action" do
    @notification.action = ""
    assert_not @notification.valid?
  end
end
