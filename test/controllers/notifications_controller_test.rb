require 'test_helper'

class NotificationsControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    sign_in users(:michael)
    get notifications_path
    assert_response :success
    assert_template "notifications/index"
  end

  test "should redirect index when not logged in" do
    get notifications_path
    assert_redirected_to new_user_session_path
  end
end
