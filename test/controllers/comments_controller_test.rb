require 'test_helper'

class CommentsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:michael)
    @other_user = users(:archer)
    @micropost = microposts(:orange)
    @comment = comments(:one)
  end

  test "should redirect create when not logged in" do
    assert_no_difference 'Comment.count' do
      post comments_path, params: {
        micropost_id: @micropost.id,
        comment: { user_id: @user.id, content: "Lorem ipsum" },
      }
    end
    assert_redirected_to new_user_session_path
  end

  test "should redirect destroy when not logged in" do
    assert_no_difference "Comment.count" do
      delete comment_path(@comment)
    end
    assert_redirected_to new_user_session_path
  end

  test "should redirect destroy for wrong comment" do
    sign_in(@other_user)
    assert_no_difference 'Comment.count' do
      delete comment_path(@comment)
    end
    assert_redirected_to root_path
  end
end
