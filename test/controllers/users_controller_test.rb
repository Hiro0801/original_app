require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:michael)
  end

  test "should get show" do
    sign_in @user
    get user_url(@user)
    assert_response :success
  end

  test "should redirect show when not logged in" do
    get user_url(@user)
    assert_response :redirect
    assert_not flash.empty?
    follow_redirect!
    assert_template "devise/sessions/new"
  end
end
