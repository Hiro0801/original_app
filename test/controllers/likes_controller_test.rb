require 'test_helper'

class LikesControllerTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:michael)
    @micropost = microposts(:orange)
    @like = likes(:two)
  end

  test "should redirect create when not logged in" do
    assert_no_difference 'Like.count' do
      post likes_path
    end
    assert_redirected_to new_user_session_path
  end

  test "should redirect destroy when not logged in" do
    assert_no_difference 'Like.count' do
      delete like_path(@micropost)
    end
    assert_redirected_to new_user_session_path
  end

  test "should redirect destroy for wrong comment" do
    sign_in @user
    assert_no_difference 'Comment.count' do
      delete comment_path(@like)
    end
    assert_redirected_to root_path
  end
end
