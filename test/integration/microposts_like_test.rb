require 'test_helper'

class MicropostsLikeTestTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:michael)
    @like = likes(:one)
    @micropost = microposts(:orange)
  end

  test "should create likes the standard way" do
    sign_in @user
    assert_difference 'Like.count', 1 do
      post likes_path, params: { user_id: @user.id, micropost_id: @micropost.id }
    end
  end

  test "should create likes with Ajax" do
    sign_in @user
    assert_difference 'Like.count', 1 do
      post likes_path, xhr: true, params: {
        user_id: @user.id,
        micropost_id: @micropost.id,
      }
    end
  end

  test "should delete likes the standard way" do
    sign_in @user
    assert_difference 'Like.count', -1 do
      delete like_path(@like)
    end
  end

  test "should delete likes with Ajax" do
    sign_in @user
    assert_difference 'Like.count', -1 do
      delete like_path(@like), xhr: true
    end
  end
end
