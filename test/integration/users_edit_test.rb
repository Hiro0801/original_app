require 'test_helper'

class UsersEditTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:michael)
  end

  test "unsuccessful edit" do
    sign_in @user
    get edit_user_registration_path
    assert_template 'devise/registrations/edit'
    patch user_registration_path, params: { user: {
      name: "",
      user_name: @user.user_name,
      email: @user.email,
    } }
    assert_template 'devise/registrations/edit'
    assert_select 'div#error_explanation'
    assert_select 'div.field_with_errors'
    patch user_registration_path, params: { user: {
      name: @user.name,
      user_name: "",
      email: @user.email,
    } }
    assert_template 'devise/registrations/edit'
    assert_select 'div#error_explanation'
    assert_select 'div.field_with_errors'
    patch user_registration_path, params: { user: {
      name: @user.name,
      user_name: @user.user_name,
      email: "",
    } }
    assert_template 'devise/registrations/edit'
    assert_select 'div#error_explanation'
    assert_select 'div.field_with_errors'
  end

  test "successful edit" do
    sign_in @user
    get edit_user_registration_path
    assert_template 'devise/registrations/edit'
    name = "Foo Bar"
    user_name = "Foo"
    email = "foo@bar.com"
    patch user_registration_path, params: { user: {
      name: name,
      user_name: user_name,
      email: email,
    } }
    assert_not flash.empty?
    assert_redirected_to root_url
    @user.reload
    assert_equal name, @user.name
    assert_equal user_name, @user.user_name
    assert_equal email, @user.email
  end
end
