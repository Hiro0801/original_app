require 'test_helper'

class PasswordResetsTest < ActionDispatch::IntegrationTest
  def setup
    ActionMailer::Base.deliveries.clear
    @user = users(:michael)
  end

  test "password resets" do
    get new_user_password_path
    assert_template 'devise/passwords/new'
    # メールアドレスが無効
    post user_password_path, params: { user: { email: "" } }
    assert_template 'devise/passwords/new'
    assert_select 'div#error_explanation'
    assert_select 'div.field_with_errors'
    # メールアドレスが有効
    post user_password_path,
         params: { user: { email: @user.email } }
    assert_equal 1, ActionMailer::Base.deliveries.size
    assert_not flash.empty?
    assert_response :redirect
    # パスワード再設定フォームのテスト
    user = assigns(:user)
    # 有効なパスワードとパスワード確認
    patch user_password_path, params: { user: {
      email: user.email,
      reset_password_token: user.reset_password_token,
      password: "foobaz",
      password_confirmation: "foobaz",
    } }
    assert_not flash.empty?
    assert_response :success
  end
end
