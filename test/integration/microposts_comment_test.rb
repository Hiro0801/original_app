require 'test_helper'

class CommentsInterfaceTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:michael)
    @other_user = users(:archer)
    @micropost = microposts(:orange)
  end

  test "comment interface" do
    sign_in @user
    get micropost_path(@micropost)
    assert_template 'microposts/show'
    # 無効な送信
    assert_no_difference 'Comment.count' do
      post comments_path, params: {
        micropost_id: @micropost.id,
        comment: { user_id: @user.id, content: "" },
      }
    end
    assert_not flash.empty?
    # 有効な送信
    assert_difference 'Comment.count', +1 do
      post comments_path, params: {
        micropost_id: @micropost.id,
        comment: { user_id: @user.id, content: "Lorem ipsum" },
      }
    end
    assert_not flash.empty?
    assert_response :redirect
    follow_redirect!
    # 投稿を削除
    first_comment = @user.comments.first
    assert_difference 'Comment.count', -1 do
      delete comment_path(first_comment)
    end
    # 違うユーザーの投稿を削除
    second_comment = @user.comments.first
    sign_in @other_user
    assert_no_difference 'Comment.count' do
      delete comment_path(second_comment)
    end
  end
end
