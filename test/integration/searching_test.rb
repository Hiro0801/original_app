require 'test_helper'

class SearchingTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:michael)
    @other_user = users(:archer)
    @micropost = microposts(:ants)
  end

  test "successfull searching" do
    sign_in @user
    get search_path
    assert_template 'static_pages/search'
    # 空欄で検索
    post search_path, params: { search: { keyword: "" } }
    assert_not flash.empty?
    assert_template 'static_pages/search'
    # 検索ワードの該当なし
    post search_path, params: { search: { keyword: "Potepan" } }
    assert_not flash.empty?
    assert_template 'static_pages/search'
    # 検索ワードの該当あり
    post search_path, params: { search: { keyword: "what" } }
    assert flash.empty?
    assert_template 'static_pages/search'
    # 名前で検索
    post search_path, params: { search: { keyword: "archer" } }
    assert flash.empty?
    assert_template 'static_pages/search'
  end
end
