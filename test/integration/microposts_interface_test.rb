require 'test_helper'

class MicropostsInterfaceTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:michael)
    @other_user = users(:archer)
  end

  test "micropost interface" do
    sign_in @user
    get new_micropost_path
    assert_template "microposts/new"
    # 無効な送信
    assert_no_difference 'Micropost.count' do
      post microposts_path, params: { micropost: { image: "" } }
    end
    assert_select 'div#error_explanation'
    # 有効な送信
    image = fixture_file_upload('/files/rails.png', 'image/png')
    content = "Lorem ipsum"
    assert_difference 'Micropost.count', 1 do
      post microposts_path, params: { micropost: { image: image, content: content } }
    end
    assert_response :redirect
    follow_redirect!
    assert_match content, response.body
    # 投稿を削除
    first_micropost = @user.microposts.paginate(page: 1).first
    assert_difference 'Micropost.count', -1 do
      delete micropost_path(first_micropost)
    end
    # 違うユーザーの投稿を削除
    second_micropost = @user.microposts.paginate(page: 1).first
    sign_in @other_user
    assert_no_difference 'Micropost.count' do
      delete micropost_path(second_micropost)
    end
  end
end
