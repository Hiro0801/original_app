require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest
  def setup
    ActionMailer::Base.deliveries.clear
  end

  test "invalid signup information" do
    get new_user_registration_path
    assert_no_difference 'User.count' do
      post user_registration_path, params: { user: {
        name: "",
        user_name: "",
        email: "user@invalid",
        password: "foo",
        password_confirmation: "bar",
      } }
    end
    assert_template 'devise/registrations/new'
    assert_select 'div#error_explanation'
    assert_select 'div.field_with_errors'
  end

  test "valid signup information with account activation" do
    get new_user_registration_path
    assert_difference 'User.count', 1 do
      post user_registration_path, params: { user: {
        name: "Example User",
        user_name: "Example",
        email: "user@example.com",
        password: "password",
        password_confirmation: "password",
      } }
    end
    assert_equal 1, ActionMailer::Base.deliveries.size
  end
end
